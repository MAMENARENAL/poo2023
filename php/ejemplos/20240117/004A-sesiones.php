<?php
session_start();

// mensaje por defecto
$mensaje = "";

// compruebo si no existe la variable de sesion
// esto solo lo hace la primera vez
if (!isset($_SESSION["aciertos"])) {
    // la primera vez que cargues la pagina
    // inicializo las variables de sesion
    $_SESSION["numeros"] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    $_SESSION['aciertos'] = []; // numeros acertados
    $_SESSION['errores'] = 0; // numero de errores
    $_SESSION['intentos'] = 0; // numero de intentos
}


// ahora quiero comprobar si he pulsado el boton de jugar
if ($_POST) {
    // compruebo si el numero existe en el array de numeros
    // y no esta en el array de aciertos
    if (
        in_array($_POST["numero"], $_SESSION["numeros"]) // compruebo si el numero existe en el array de numeros
        &&
        !in_array($_POST["numero"], $_SESSION["aciertos"])  // compruebo si el numero no ha sido acertado anteriormente
    ) {
        // añado al array de aciertos el numero acertado
        $_SESSION['aciertos'][] = $_POST["numero"];

        // si gano sera aqui donde lo puedo saber
        if (count($_SESSION['aciertos']) == 10) {
            session_destroy();
            $mensaje = "Has ganado";
        }
    } else {
        // si no he acertado el numero sumo uno al contador de errores
        $_SESSION['errores']++;
    }
    // tanto si acierto como si fallo
    // sumo uno al contador de intentos
    $_SESSION['intentos']++;
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <form method="post">
        <div>
            <label for="numero">Numero</label>
            <input type="number" name="numero" id="numero" required>
        </div>

        <div>
            <button type="submit">Jugar</button>
        </div>
    </form>
    <?php
    // mostrar los numeros acertados
    echo "<h2>Numero acertados</h2>";
    foreach ($_SESSION['aciertos'] as $numero) {
        echo $numero . "<br>";
    }
    // mostrar errores
    echo "<h2>Errores</h2>";
    echo $_SESSION['errores'];

    // mostrar intentos
    echo "<h2>Intentos</h2>";
    echo $_SESSION['intentos'];

    ?>
    <br>
    <?= $mensaje ?>
</body>

</html>