<?php
// Crear un formulario que me permita introducir un numero
// almacenar ese numero en una cookie 
// y mostrarlo por pantalla
// muestro el numero introducido y el introducido anteriormente


// si he pulsado el boton de enviar
if ($_POST) {
    // almaceno el numero introducido
    // en una cookie llamada cnumero
    // durante 1 hora
    setcookie(
        "cnumero", //  nombre
        $_POST["numero"],   // valor
        time() + 3600   // tiempo vida
    );
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <form method="post">
        <input type="number" name="numero">
        <button>Enviar</button>
    </form>
    <h2>Datos introducidos actualmente</h2>
    <div>Numero <?= $_POST["numero"] ?? "" ?></div>
    <h2>Datos introducidos anteriormente</h2>
    <div>Numero <?= $_COOKIE["cnumero"] ?? "" ?></div>

</body>

</html>