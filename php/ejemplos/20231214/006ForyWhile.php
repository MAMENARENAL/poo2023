<?php
// crear una variable llamada numero con un numero valor entero
// realizr la tabla de multiplicar de ese numero desde o hasta 10 con un for
$numero=4;
for ($i = 0; $i <= 10; $i++) {
    echo "$numero x $i = " . $numero * $i . "<br>";
}   

// hacer lo mismo que en el ejercicio anterior con un while
$numero = 4;
$i = 0;

while ($i <= 10) {
    echo "$numero x $i = " . $numero * $i . "<br>";
    $i++;
}

?>