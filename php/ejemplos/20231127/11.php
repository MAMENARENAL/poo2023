<?php

$tiradas = [];
// simular cada tirada de dado utilizando  la funcion
// mt_rand(1,6)


// $tiradas = [
//     [
//         "dado1" => mt_rand(1,6),
//         "dado2" => mt_rand(1, 6),
//     ],
//     [
//         "dado1" => mt_rand(1, 6),
//         "dado2" => mt_rand(1, 6),
//     ],
// ];

$tiradas[] = [
    "dado1" => mt_rand(1, 6),
    "dado2" => mt_rand(1, 6),
];

$tiradas[] = [
    "dado1" => mt_rand(1, 6),
    "dado2" => mt_rand(1, 6),
];

$tiradas[] = [
    "dado1" => mt_rand(1, 6),
    "dado2" => mt_rand(1, 6),
];

// array_push($tiradas, [
//     "dado1" => mt_rand(1, 6),
//     "dado2" => mt_rand(1, 6),
// ]);

// array_push($tiradas, [
//     "dado1" => mt_rand(1, 6),
//     "dado2" => mt_rand(1, 6),
// ]);

// array_push($tiradas, [
//     "dado1" => mt_rand(1, 6),
//     "dado2" => mt_rand(1, 6),
// ]);

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <table>
        <tr>
            <td>Dado1</td>
            <td>Dado2</td>
        </tr>
        <tr>
            <td><?= $tiradas[0]["dado1"] ?></td>
            <td><?= $tiradas[0]["dado2"] ?></td>
        </tr>
        <tr>
            <td><?= $tiradas[1]["dado1"] ?></td>
            <td><?= $tiradas[1]["dado2"] ?></td>
        </tr>
        <tr>
            <td><?= $tiradas[2]["dado1"] ?></td>
            <td><?= $tiradas[2]["dado2"] ?></td>
        </tr>
    </table>
</body>

</html>