<?php
// variables a utilizar
$a = 10;
$b = 3;
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Operadores</title>
</head>

<body>
    <?php
    // Quiero que coloquemos los resultados de las siguientes operaciones
    // a+b
    // a-b
    // a*b
    // a resto b
    // a elevado b
    // a dividido entre b
    // a es igual a b
    // a menor o igual que b

    // colocamos una tabla en donde la primera columa es el nombre
    // de la operacion
    // en la segunda columna el resultado

    // procesamiento
    $suma = $a + $b;
    $resta = $a - $b;
    $producto = $a * $b;
    $resto = $a % $b;
    $elevado = $a ** $b;
    $cociente = $a / $b;

    if ($a == $b) {
        $iguales = "Si, son iguales";
    } else {
        $iguales = "No son iguales";
    }

    if ($a <= $b) {
        $menores = "a es menor o igual que b";
    } else {
        $menores = "a no es menor o igual que b";
    }

    // mostrar resultados
    ?>
    <table border="1">
        <tr>
            <td>Operaciones</td>
            <td>Resultados</td>
        </tr>
        <tr>
            <td>Suma</td>
            <td><?php echo $suma; ?></td>
        </tr>
        <tr>
            <td>Restar</td>
            <td><?= $resta ?></td>
        </tr>
        <tr>
            <td>Producto</td>
            <td><?= $producto ?></td>
        </tr>
        <tr>
            <td>Resto</td>
            <td><?= $resto ?></td>
        </tr>
        <tr>
            <td>Elevado</td>
            <td><?= $elevado ?></td>
        </tr>
        <tr>
            <td>Cociente</td>
            <td><?= $cociente ?></td>
        </tr>
        <tr>
            <td>A igual a B</td>
            <td>
                <?= $iguales ?>
            </td>
        </tr>
        <tr>
            <td>A menor o igual que B</td>
            <td>
                <?= $menores ?>
            </td>
        </tr>
    </table>
</body>

</html>