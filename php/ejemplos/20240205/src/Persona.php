<?php

namespace src;

class Persona{
    private ?string $nombre;
    private ?int $edad;

     public function __construct()  {
        $this->nombre = "null";    
       $this->edad = 0;
    }
    
    public function mostrar(): string
     {
        $salida="<ul>";
        $salida .= "<li>Nombre: " . $this->nombre . "</li>";
        $salida .= "<li>Edad: " . $this->edad . "</li>";
        $salida .= "</ul>";
        return $salida;
    }
    public function __toString(): string
    {
        return $this->mostrar();
    }

    /**
     * Get the value of edad
     */
    public function getEdad(): ?int
    {
        return $this->edad;
    }

    /**
     * Set the value of edad
     */
    public function setEdad(int $edad): self
    {
        $this->edad = $edad;

        return $this;
    }


    

    /**
     * Get the value of nombre
     */
    public function getNombre(): string
    {
        return $this->nombre;
    }

    /**
     * Set the value of nombre
     */
    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }
}