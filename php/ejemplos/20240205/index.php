<?php

use src\Cliente;
use src\Directivo;
use src\Empleado;


require "autoload.php";

$empleado = [];

$empleado[0] = new Empleado();
$empleado[0]->setNombre("Ana");
$empleado[0]->setEdad(30);
$empleado[0]->calcularSueldo(54);
// si no tenemos toString
// echo $empleado[0]->mostrar();
// si hemos colocado toString

echo $empleado[0];

$empleado[1] = new Empleado();
$empleado[1]->setNombre("Luisa");
$empleado[1]->setEdad(45);
$empleado[1]->calcularSueldo(45);
echo $empleado[1]->mostrar();

var_dump($empleado);

$clientes = [];
$clientes[0] = new Cliente();
$clientes[0]->setNombre("Cesar");
$clientes[0]->setEdad(35);
$clientes[0]->setNombreEmpresa("Alpe");
$clientes[0]->setTelefono("t1");
echo $clientes[0];

var_dump($clientes);


$directivos = [];

$directivos[0] = new Directivo();
$directivos[0]->setNombre("Jose");
$directivos[0]->setEdad(30);
$directivos[0]->calcularSueldo(5);
$directivos[0]->setCategoria("A");
echo $directivos[0];

