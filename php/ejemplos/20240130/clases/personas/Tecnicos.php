<?php
// colocamos el espacio de nombres
namespace clases\personas;
// creamos la clase
class Tecnicos
{
    // atributos de la clase
    // public |private|protected  tipoDato identificador;
    public string $nombre;
    private string $iniciales;
    protected float $sueldo;
    public int $horas;

    // metodo constructor
    public function __construct(string $nombre)
    {
        $this->nombre = $nombre;
        $this->iniciales = "";
        $this->sueldo = 0;
        $this->horas = 0;
    }

    // metodos de la clase


    /**
     * devuelve las iniciales
     */
    public function getIniciales(): string
    {
        return $this->iniciales;
    }

    /**
     * Asignar las iniciales
     *
     * @return  self
     */
    public function setIniciales($iniciales): self
    {
        $this->iniciales = $iniciales;

        return $this; // trabajar en modo fluent
    }

    private function calcularSueldo(): void
    {
        $this->sueldo = $this->horas * 10;
    }

    public function getSueldo(): float
    {
        $this->calcularSueldo();
        return $this->sueldo;
    }
}
