<?php
// precarga de clases
spl_autoload_register(function ($clase) {
    include "clases/" . $clase . '.php';
});

// creo un objeto de tipo coche

$ford1 = new Coche();

// creo otro objeto de tipo coche
$ford2 = new Coche("rojo", 5, "ford");


// si quiero poner la marca al coche 1
// $ford1->marca="ford"; // da error porque es private
$ford1->setMarca("ford");


// modifico la gasolina
//$ford1->gasolina = 10; // da error porque es privado
$ford1->setGasolina(10);

//obtengo la gasolina
//echo $ford1->gasolina; // da error porque es privado
echo $ford1->getGasolina();


$ford2->llenarTanque(10);

var_dump($ford1);
var_dump($ford2);
