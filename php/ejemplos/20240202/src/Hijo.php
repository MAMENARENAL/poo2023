<?php
namespace src;

    class Hijo extends Padre
{
    public string $tipo;
    public function __construct(string $nombre, string $apellidos, int $edad, int $altura)
    {
        $this->tipo = "hijo";
        $this->altura = 0; // al ser la altura protected, puedo acceder pero es la misma que la de la superclase
        parent::__construct($nombre, $apellidos, $edad, $altura);
    }
    public function presentarme(): string
    {
        return "Soy el hijo";
    }

    public function getTipo(): string
    {
        return $this->tipo;
    }

    public function getTipoPadre(): string
    {
        return parent::getTipo();
    }

    public function asignar(string $nombre, string $apellidos)
    {
        parent::asignar($nombre, $apellidos);
        $this->tipo = "hijo con datos";
        $this->setTipo('padre con hijo con datos');
    }
}
