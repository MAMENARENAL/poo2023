<?php
namespace clases;

class Persona
{
    // es importante tiene que tener tantos atributos
    // como campos tenga la tabla
    public int $idPersona;
    public string $nombre;
    public int $edad;

 

    /**
     * Get the value of idPersona
     */
    public function getIdPersona(): int
    {
        return $this->idPersona;
    }

    /**
     * Set the value of idPersona
     */
    public function setIdPersona(int $idPersona): self
    {
        $this->idPersona = $idPersona;

        return $this;
    }

    /**
     * Get the value of nombre
     */
    public function getNombre(): string
    {
        return $this->nombre;
    }

    /**
     * Set the value of nombre
     */
    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get the value of edad
     */
    public function getEdad(): int
    {
        return $this->edad;
    }

    /**
     * Set the value of edad
     */
    public function setEdad(int $edad): self
    {
        $this->edad = $edad;

        return $this;
    }

    public function __toString()
     {
        $salida="<ul>";
        $salida="<li> Id persona: ".$this->idPersona."<li>";
        $salida.="<li> Nombre: ".$this->nombre."<li>";
        $salida.="<li> Edad: ".$this->edad."<li>";
        $salida."</ul>";
        return $salida;

    }
}
