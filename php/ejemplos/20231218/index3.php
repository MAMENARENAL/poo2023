<?php

require_once "003-arrays.php";

require_once "bbdd.php";

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="estilos.css">
</head>

<body>
    <?= mostrarDatos($alumnos) ?>
    <?= mostrarDatosV1($alumnos) ?>
    <?= mostrarDatosV2($alumnos) ?>

    <?= mostrarDatosCabecera($alumnos) ?>
    <?= mostrarDatosCabecera($datos) ?>

</body>

</html>