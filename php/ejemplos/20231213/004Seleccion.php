<?php

// Crear una variable llamada numero que puede valer entre (calcularla aleatoriamente) 
// 1-100 (numero entero)
// Imprimir
// “alpe” si el numero es múltiplo de 3 
// “POO” si el numero es múltiplo de 5
// “alpe POO” si el numero es múltiplo de 3 y de 5

$numero = rand(1, 100);
echo "<h2>{$numero}</h2>";

if ($numero % 3 == 0 && $numero % 5 == 0) {
    echo "alpe POO";
} elseif ($numero % 3 == 0) {
    echo "alpe";
} elseif ($numero % 5 == 0) {
    echo "POO";
}

// realizarlo con switch

switch (true) {
    case ($numero % 3 == 0 && $numero % 5 == 0):
        echo "alpe POO";
        break;
    case ($numero % 3 == 0):
        echo "alpe";
        break;
    case ($numero % 5 == 0):
        echo "POO";
        break;
}

// otra opcion

if ($numero % 3 == 0) {
    echo "alpe";
}

if ($numero % 5 == 0) {
    echo "POO";
}
