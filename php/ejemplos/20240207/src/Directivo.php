<?php
namespace src;

class Directivo extends Empleado {
    protected ?string $categoria;

public function mostrar(): string
{
    $salida = "<ul>";
    $salida .= "<li>Nombre:" . $this->getNombre() . "</li>";
    $salida .= "<li>Edad:" . $this->getEdad() . "</li>";
    $salida .= "<li>Categoria:" . $this->categoria . "</li>";
    $salida .= "<li>Sueldo:" . $this->getSueldo(). "</li>";
    $salida .= "</ul>";
    return $salida;
}

public function __construct()
{
    parent::__construct();
    $this->setCategoria(null);
    $this->propiedadesAsignacionMasiva = ["nombre", "edad","categoria"];
}
    


    /**
     * Set the value of categoria
     *
     * @param ?string $categoria
     *
     * @return self
     */
    public function setCategoria(?string $categoria): self
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get the value of categoria
     *
     * @return ?string
     */
    public function getCategoria(): ?string
    {
        return $this->categoria;
    }
public function asignar(array $datos): self
{
    parent::asignar($datos);
    $this->setCategoria($datos["categoria"]);
    return $this;
}

}