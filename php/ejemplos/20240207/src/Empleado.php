<?php

namespace src;

class Empleado extends Persona
{
    private ?float $sueldo;

    // sobreescribir el metodo mostrar
    public function mostrar(): string
    {
        $salida = "<ul>";
        $salida .= "<li>Nombre:" . $this->getNombre() . "</li>";
        $salida .= "<li>Edad:" . $this->getEdad() . "</li>";
        $salida .= "<li>Sueldo:" . $this->sueldo . "</li>";
        $salida .= "</ul>";
        return $salida;
    }
    

    public function __construct()
    {
        parent::__construct();
        $this->sueldo = null;
        // en el constructor indico que campos quiero que se asignen por defecto
        // desde un formulario
        $this->propiedadesAsignacionMasiva = ["nombre", "edad"];
    }
    public function calcularSueldo(int $horas): void
    {
        $this->sueldo = $horas * 10;
    }
// añadir el get de sueldo
public function getSueldo(): ?float
{
    return $this->sueldo;
}   
// se lo quito y lo hereda del padre con metodos de asignacion masiva
// public function asignar(array $datos): self
// {
//     $this->setNombre($datos["nombre"]);
//     $this->setEdad($datos["edad"]);
//     $this->calcularSueldo($datos["horas"]);
//     return $this;
// }
public function asignar (array $datos): self
{
    parent::asignar($datos);
    $this->calcularSueldo($datos["horas"]);
    return $this;
}


}
