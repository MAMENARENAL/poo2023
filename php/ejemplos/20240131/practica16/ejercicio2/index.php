<?php

use clases\Persona;

spl_autoload_register(function ($clase) {
    include $clase . '.php';
});

$objeto = new Persona("Juan", "Perez", "12345678", 2000, "España", 'h');
$objeto1 = new Persona("Pedro", "Gonzalez", "12347878", 2000, "España", 'h');

echo $objeto->imprimir();
echo $objeto1->imprimir();
