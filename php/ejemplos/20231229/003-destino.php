<?php
// si no ha cargado el formulario le obligo a cargarlo
if (!$_POST) {
    header("Location: 003-formularioTresBotones.php");
}
//var_dump($_POST);

// funcion que recibe un texto y un caracter
// debe retornar el numero de veces que aparece el caracter
// y el texto debe modificarle sustituendo el caracter por un guion
function caracteres(string &$texto, string $caracter): int
{
    $resultado = 0;
    // opcion 1
    // for ($i = 0; $i < strlen($texto); $i++) {
    //     if ($texto[$i] == $caracter) {
    //         $texto[$i] = "-";
    //         $resultado++;
    //     }
    // }

    // opcion 2
    $resultado = substr_count($texto, $caracter);
    $texto = str_replace($caracter, "-", $texto);
    return $resultado;
}

$boton = $_POST["boton"] ?: "ninguno";
$texto = $_POST["texto"] ?: "";
$resultado = "";

// realizo una copia del texto
$textoSalida = $texto;

switch ($boton) {
    case 'a':
    case 'b':
    case 'c':
        $resultado = caracteres($textoSalida, $boton);
        break;
    default:
        $resultado = "el caracter no es valido";
        break;
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <div>
        <?= $texto ?>
    </div>
    <div>El caracter buscado es <?= $boton ?></div>
    <div>
        <?= $resultado ?>
    </div>
    <div>
        <?= $textoSalida ?>
    </div>
</body>

</html>