<?php
function caracteres(string &$texto, string $caracter): int
{
    $resultado = 0;
$texto=str_replace($caracter, "-", $texto);
    $resultado = substr_count($texto, $caracter);
    return $resultado;
}

// LOGICA DE CONTROL
// PRIMERO VER SI CARGA LOS DATOS
if($_POST)    {
    
    $boton = $_POST["boton"] ?: "ninguno";
    $texto = $_POST["texto"] ?: "";
    $resultado = "";

// realizo una copia del texto
$textoSalida=$texto;

switch ($boton) {
    case 'a':
    case 'b':
    case 'c':
        $resultado = caracteres($texto, $boton);
        break;
    default:
        $resultado = "el caracter no es valido";
        break;
}
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <form  method="post">
        <div>
            <label for="texto">Texto</label>
            <input type="text" name="texto" id="texto" required placeholder="escribe una frase">
        </div>
        <div>
            <button name="boton" value="a">a</button>
            <button name="boton" value="b">b</button>
            <button name="boton" value="c">c</button>
        </div>
    </form>
    <?php
    if($_POST){
    ?>
    
    <div><?= $texto ?></div>
    <div>El caracter buscado es <?= $boton ?></div>
    <div> <?= $resultado ?> </div>
    <div> <?= $textoSalida ?> </div>
    <?php
    }
    ?>
</body>

</html>