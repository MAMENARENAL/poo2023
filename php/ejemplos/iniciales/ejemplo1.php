<?php
/*
crear variables inicializandolas
*/

// creo una variable y la inicializo a 1
$numero = 1;

$texto = "hola"; // creando una variable de tipo string

/*
fin de creacion de variables
*/
// $numero=1
$numero = $numero + 1;
// $numero=2
$numero++;
// $numero=3
++$numero;
// $numero=4
$numero += 2; // $numero = $numero + 2;
// $numero=6

echo "La variable numero vale " . $numero . "<br>";
echo "La variable numero vale $numero<br>";
echo 'La variable numero vale $numero<br>';
echo "La variable numero vale {$numero}<br>";
