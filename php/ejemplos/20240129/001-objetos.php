<?php

use clases\numero\Numeros;
use clases\texto\Cadenas;

include "autoload.php";

$objeto1 = new Numeros(10);
$objeto2 = new Cadenas("hola clase");

$objeto3 = new Numeros(20);

var_dump($objeto1);
var_dump($objeto2);
var_dump($objeto3);
