<?php

namespace clases\herencia;

class Animal // esta va a ser la clase padre (superclase)
{
    public string $nombre;
    public float $peso;
    public string $color;

    public function __construct(string $nombre = "", float $peso = 0, string $color = "")
    {
        $this->nombre = strtoupper($nombre);
        $this->peso = $peso;
        $this->color = $color;
    }

    public function descripcion(): string
    {
        return "{$this->nombre} tiene {$this->peso} kilos y es de color {$this->color}.";
    }
}
