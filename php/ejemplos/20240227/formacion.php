<?php

// cargando la clase en el espacio de nombres actual
use clases\Aplicacion;
use clases\Header;
use clases\Modelo;
use clases\Pagina;


// definiendo el autoload
spl_autoload_register(function ($clase) {
    include $clase . '.php';
});


// instanciando la clase
$aplicacion = new Aplicacion();
$favoritos = new Modelo($aplicacion->db);
$favoritos->query("select * from favoritos where categorias='formacion'");
$cabecera = Header::ejecutar([
    "titulo" => "Formacion",
    "subtitulo" => $aplicacion->configuraciones['autor'],
    "salida" => "Paginas relativas a formacion"
]);

Pagina::comenzar();
?>
<?= $favoritos->gridViewBotones(); ?>
<?php
Pagina::terminar([
    "titulo" => "formacion",
    "cabecera" => $cabecera,
    "pie" => "Creado por: " . $aplicacion->configuraciones['autor']
]);
