<?php
// crear una funcion que reciba 
// como primer argumento una etiqueta html
// como segundo argumento el contenido de la etiqueta
// debe devolver una etiqueta html con el contenido
// de la etiqueta


/**
 * Crea una etiqueta HTML con la etiqueta y el contenido proporcionados.
 *
 * @param string $etiqueta La etiqueta HTML.
 * @param string $contenido El contenido dentro de la etiqueta HTML.
 * @return string La etiqueta HTML con la etiqueta y el contenido.
 */
function crearEtiqueta( string $etiqueta,string $contenido):string
{
    return "<{$etiqueta}>{$contenido}</{$etiqueta}>";
}

echo crearEtiqueta("p","Hola mundo");
?>