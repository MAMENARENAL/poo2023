<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    // comprobar si he pulsado el boton de enviar
    if (isset($_GET["mandar"])) {
        // si para analizar los valores del array $_GET
        // utilizo el operador ternario contraido
        // si las cajas de texto estan vacias entra a verdadero
        // $nombre = $_GET["nombre"] ?? "no conocido";
        // $apellidos = $_GET["apellidos"] ?? "no conocidos";
        // $edad = $_GET["edad"] ?? "no conocida";

        // si analizo los valores del array $_GET
        // con el operador elvis
        // si las cajas de texto estan vacias entra a falso
        $nombre = $_GET["nombre"] ?: "no conocido";
        $apellidos = $_GET["apellidos"] ?: "no conocidos";
        $edad = $_GET["edad"] ?: "no conocida";
        var_dump($nombre, $apellidos, $edad);
    } else {
        require "_formulario1.php";
    }
    ?>
</body>

</html>