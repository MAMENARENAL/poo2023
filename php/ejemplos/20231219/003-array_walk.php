<?php

$numeros = [
    1, 100, 12, 23
];

$salida = "";

// array_walk($numeros, function (&$numero){
//     $numero = "<div>{$numero}</div>";
// });

// $salida = implode("", $numeros);

array_walk($numeros, function ($numero) use (&$salida) {
    $salida .= "<div>{$numero}</div>";
});

var_dump($salida);
