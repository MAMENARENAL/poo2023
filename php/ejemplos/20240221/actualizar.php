<?php
session_start();
require_once "funciones.php";

// cargo los parametros de aplicacion
$parametros = require_once("parametros.php");

// desactivar errores
controlErrores();


// creo un array con los elementos
// que quiero que tenga el menu
$elementosMenu = [
    "Inicio" => "index.php",
    "Insertar" => "insertar.php"
];

// preparo el menu
echo menu($elementosMenu);

// conexion a base de datos
$conexion = @new mysqli(
    $parametros["bd"]["servidor"],
    $parametros["bd"]["usuario"],
    $parametros["bd"]["password"],
    $parametros["bd"]["nombreBd"]
);

// compruebo si la conexion es correcta
if ($conexion->connect_error) {
    die("Error de conexión: " . $conexion->connect_error);
}

// inicializo la salida de la vista
$salida = "";

// comprobar si vengo de pulsar sobre el boton editar del gridView
// por lo tanto me llega el id por get
if(isset($_GET["id"])){
    // leer el ide sesion
    $_SESSION["id"] = $_GET["id"];
    // tengo que leer el registro con ese id
// preparo la consulta para obtener los datos del libro a modificar
    $sql= "SELECT * FROM libros WHERE id = ({$_GET["id"]})";
   // lanzo la consulta
   $resultado= $conexion->query($sql);
   $datos= $resultado->fetch_assoc();

}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1> Editar</h1>
    <?= require "_form.php" ?>
    <?= $salida ?>
    
</body>
</html>