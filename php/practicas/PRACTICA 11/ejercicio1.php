<?php
// desarrollar una funcion que acepte como parámetros
// valor mínimo
// valor máximo
// numero de valores
// la función debe generar un vector con números aleatorios
// que los devolvera ordenados de forma ascendente

/**
 * Genera un array de números aleatorios entre un valor mínimo y máximo dado.
 *
 * @param int $min El valor mínimo para los números aleatorios.
 * @param int $max El valor máximo para los números aleatorios.
 * @param int $num0 El tamaño del array que se va a generar.
 * @return array El array generado de números aleatorios.
 */

function ejercicio1($min, $max, $num0){
    $local=array(); // array donde se coloca el resultado
    for($i=0;$i<$num0;$i++){
        $local[$i]=mt_rand($min,$max);
    }
    // devolver el array
return $local;
}



?>