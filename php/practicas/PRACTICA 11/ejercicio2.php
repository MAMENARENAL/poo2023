<?php
// desarrollar una funcion que acepte como parámetros
// valor mínimo
// valor máximo
// numero de valores
// la función debe generar un vector con números aleatorios
// que los devolvera ordenados de forma ascendente
// modificar la funcion anterior para que acepte 4 argumentos
// valor mínimo
// valor máximo
// numero de elementos
// array donde se almacena


/**
 * Genera un array de números aleatorios entre $min y $max.
 *
 * @param int $min el valor mínimo para los números aleatorios
 * @param int $max el valor máximo para los números aleatorios
 * @param int $num0 la cantidad de elementos a generar en el array
 * @param array $salida el array donde se almacena el resultado
 * @return array el array generado de números aleatorios
 */

function ejercicio2($min, $max, $num0, $salida){
    $salida=array(); // array donde se coloca el resultado
    // bucle para rellenar el array
    for($i=0;$i<$num0;$i++){
        $salida[$i]=mt_rand($min,$max);
    }
    // devolver el array
    return $salida;
}

?>