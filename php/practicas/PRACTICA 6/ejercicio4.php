<?php

// hacer que cada vez que cargue una pagina web debe colocar un numero entero entre 1 y 10
// utilizar la funcion mt_rand
// si el numero es par o impar indicarlo con un mensaje
$num =mt_rand(1,10);
// nos muestra en pantalla un mensaje si el numero es par o impar
echo $num;
if ($num % 2 == 0) {
    echo "El número es par";
} else {
    echo "El número es impar";
}

?>