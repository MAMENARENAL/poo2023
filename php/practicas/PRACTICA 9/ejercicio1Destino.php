<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
 <!-- Este es el metodo largo
     -->
HOLA <?php echo $_GET['nombre']; ?><br>
Tu email es: <?php echo $_GET['email']; ?>

<!-- Este es el metodo corto -->

HOLA <?= $_GET['nombre']; ?>
Tu email es: <?= $_GET['email']; ?>
</body>
</html>