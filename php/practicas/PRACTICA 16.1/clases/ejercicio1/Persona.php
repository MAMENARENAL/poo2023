<?php
namespace clases\ejercicio1;
class Persona
{
    public string $nombre ;
    public int $edad;
    public bool $activo;

    // definimos un constructor que inicializa estos dos atributos
    public function __construct()
    {
        $this->nombre = "";
        $this->edad = 0;
    }


// creamos un metodo llamado mostrarInformacion que muestra el nombre y la edad de la persona
public function mostrarInformacion(): string
{
    $salida= "<h2>Datos</h2><br>";
    $salida.= "Nombre: " . $this->nombre . "<br>";
    $salida.="Edad: " . $this->edad . "<br>";
     return $salida;
}
// creamos un metodo para activar el usuario
public function activarUsuario(): void
{
    $this->activo = true;
}
// creamos un metodo para cambiar la edad
public function cambiarEdad($nuevaEdad): void
{
    $this->edad = $nuevaEdad;
}
}