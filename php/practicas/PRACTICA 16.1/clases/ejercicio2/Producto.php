<?php

namespace clases\ejercicio2;

class Producto
{
    // propiedades
    public int $idProducto;
    public string $nombre;
    public float $precio;
    public int $stock;
    public string $descripcion;
    public float $descuento;
    public float $precioConDescuento;
    // creamos el constructor

    public function __construct(int $idProducto, string $nombre, float $precio, int $stock, string $descripcion)
    {
        $this->idProducto = $idProducto;
        $this->nombre = $nombre;
        $this->precio = $precio;
        $this->stock = $stock;
        $this->descripcion = $descripcion;
    }
    // creamos metodo mostrar detalles que nos da los detalles del producto

    public function mostrarDetalles(): string
    {
        $salida = "<h2>Mostrar Detalles</h2> <br>";
        $salida .= "Nombre: " . $this->nombre . "<br>";
        $salida .= "Precio: " . $this->precio . "<br>";
        $salida .= "Stock: " . $this->stock . "<br>";
        $salida .= "Descripción: " . $this->descripcion . "<br>";
        return $salida;
    }

    public function __toString()
    {
        return $this->mostrarDetalles();
    }
    // creamos el metodo actualizar stock

    public function actualizarStock(int $cantidad): self
    {
        $this->stock = $cantidad;
        return $this;
    }
    // creamos el metodo calcula precio descuento
    public function calcularPrecioDescuento($descuento): float
    {
        $precioConDescuento = $this->precio - ($this->precio * $descuento / 100);
        return $precioConDescuento;
    }
}
