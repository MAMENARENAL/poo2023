<?php
// colocar en una variable un numero directamente
// debemos devolver la suma de los numeros impares desde 0 hasta el numero
// por ejemplo si intgresamos un 8
// el resultado será Suma = 1 + 3 + 5 + 7 = 16

$numero = 8;
$suma = 0;

for ($i = 0; $i <= $numero; $i++) {
    if ($i % 2 != 0) {
        $suma += $i;
    };
}   
echo "Suma = $suma";
?>
